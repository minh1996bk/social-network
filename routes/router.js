module.exports = function(io) {
    var express = require('express')
    var router = express.Router()

    var userController = require('../controllers/userController');
    var aboutLogin = require('../controllers/aboutLogin');
    var aboutRegister = require('../controllers/aboutRegister');
    var aboutLogout = require('../controllers/aboutLogout');
    var sr = require('../controllers/socketRelated')(io);


    router.get('/', aboutLogin.home);

    router.get('/login', aboutLogin.login);

    router.post('/login', aboutLogin.doLogin);




    router.get('/register', aboutRegister.register);

    router.post('/register', aboutRegister.doRegister);

    router.get('/logout', aboutLogout.logout);






    router.post('/createPost', sr.createPost);

    router.post('/comment', sr.comment);

    router.post('/sendFriendRequest', sr.sendFriendRequest);

    router.post('/acceptFriendRequest', sr.acceptFriendRequest);





    router.get('/profile/:id', userController.viewProfile);

    router.get('/getComments/:id', userController.getComments);

    router.get('/getFriendTips', userController.getFriendTips);

    router.get('/getFriendRequest', userController.getFriendRequest);

    router.get('/getPosts', userController.getPosts);

    return router
}