$(document).ready(function() {
    $.get('/getFriendRequest', function(requests) {
        htm = "";
        requests.forEach(function(req) {
            htm += "<div class='friend-request-element'>";
                htm += "<img src='/images/user-icon.jpg' class='rounded-circle icon-user'>";
                htm += "<div>";
                    htm += "<input type='hidden' value='" + req._id +"'>";
                    htm += "<p><a href='/profile/" + req._id + "'>" + req.fullname + "</a></p>";
                    htm += "<button class='accept-friend-btn'>Accept</button>";
                htm += "</div>";
            htm += "</div>";
        });

        $('#friend-request-drop-down-list').append(htm);

        
    });
    $('#noti-related-container').on('click', '#friend-request-noti', function() {
        document.getElementById("friend-request-drop-down-list").classList.toggle("show");
        return false;
    })
   

    $('#noti-related-container').on('click', function() {
        $('.friend-request-element').on('click', function() {
            var friendId = $(this).parent().find('input').val();
            $.post('/acceptFriendRequest', {
                friendId: friendId
            });
            this.remove();
            return false;
        })

        // var target = document.getElementsByClassName('accept-friend-btn');
        // var friendId = target.parent().find('input').val();
        // $.post('/accepteFriendRequest', {
        //     friendId: friendId
        // });
        // target.remove();
        // return false;

        
    })
})