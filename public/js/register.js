$(document).ready(function() {
    $('#register-form').submit(function() {
        var username = $('#username').val();
        var password = $('#password').val();
        var fullname = $('#name').val();
        $.post('/register', {
            username: username,
            password: password,
            fullname: fullname
        }, function(data, status) {
            console.log(status);
            if (status === 'success') {
                window.location = '/';
            } else {
                window.alert('Register fail! Try again');
            }
            
        });
        return false;
    });
});