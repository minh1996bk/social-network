$(document).ready(function() {
    $.get('/getFriendTips', function(data, status) {
        var htm = "<div class='friend-tips-box'>";
        htm += "<p>Maybe you know ...</p><br>";

        data.forEach(function(people) {
            htm += "<div class='row element-friend-tip'>";
                htm += "<img src='/images/user-icon.jpg' class='rounded-circle icon-user'>";
                htm += "<div class='text-element-friend-tip'>";
                    htm += "<input type='hidden' value='" + people._id +"'>";
                    htm += "<p><a href='/profile/" + people._id + "'>" + people.fullname + "</a></p>";
                    htm += "<button class='add-friend-btn'>Add friend</button>";
                htm += "</div>";
            htm += "</div>";
        });
        htm += "_________";


        htm +="</div>"
        $('#friend-tip-list').append(htm);
    });


    $('#friend-tip-list').on('click', '.add-friend-btn', function() {
  
        var friendId = $(this).parent().find('input').val();
        var div = $(this).parent().parent();
        div.remove();
        $.post('/sendFriendRequest', {
            friendId: friendId
        });
        return false;
    });


});