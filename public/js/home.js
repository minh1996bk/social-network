$(document).ready(function() {



    function displayComment(cmtBox, comment) {

        // Display icon-user
        var iconUserImg = document.createElement("img");
        iconUserImg.setAttribute('src', '/images/user-icon.jpg');
        iconUserImg.setAttribute('class', 'rounded-circle icon-user-small');
        // End disply icon-user

        // Display content of comment
        var nameTag = document.createElement("A");
        nameTag.setAttribute('class', 'cmt');
        nameTag.setAttribute('href', '/profile/' + comment.userId._id);
        nameTag.innerHTML = comment.userId.fullname;
       
        var contentP = document.createElement('p');
        contentP.innerHTML = comment.content;

        var cmtContentDiv = document.createElement("div");
        cmtContentDiv.setAttribute('class', 'row comment-box');
        cmtContentDiv.appendChild(nameTag);
        cmtContentDiv.appendChild(contentP);

        // Display like answer
        var likeTag = document.createElement("A");
        likeTag.setAttribute('href', '#');
        likeTag.innerHTML = 'Like';

        var answerTag = document.createElement("A");
        answerTag.setAttribute('href', '#');
        answerTag.innerHTML = 'Answer';

        var space = document.createElement('h6');
        space.innerHTML = '<pre> . </pre>';

        var timeP = document.createElement('p');
        timeP.innerHTML = comment.time;

        var likeAnswerTimeDiv = document.createElement("div");
        likeAnswerTimeDiv.setAttribute('class', 'like-answer-time row');
        likeAnswerTimeDiv.appendChild(likeTag);
        likeAnswerTimeDiv.appendChild(space);
        likeAnswerTimeDiv.appendChild(answerTag);
        likeAnswerTimeDiv.appendChild(timeP);
        // End display

        var contentLikeAnswerTimeDiv = document.createElement("div");
        contentLikeAnswerTimeDiv.setAttribute('class', 'content-like-answer-time');

        var dmDiv = document.createElement('div');
        dmDiv.appendChild(cmtContentDiv);

        contentLikeAnswerTimeDiv.appendChild(dmDiv);
        contentLikeAnswerTimeDiv.appendChild(likeAnswerTimeDiv);

        // End display content of comment



        
        var cmtDiv = document.createElement('div');
        cmtDiv.setAttribute('class', 'row icon-cmt-box');
        cmtDiv.appendChild(iconUserImg);
        cmtDiv.appendChild(contentLikeAnswerTimeDiv);
        
        cmtBox.append(cmtDiv);
    }

    $('.get-comment-btn').on('click', function() {  
        var getCommentable = $(this).parent().find('input').val();
        
        if (getCommentable == 'false') {
            return false;
        }
        var postId = $(this).parent().parent().find('input').val();
        var path = '/getComments/' + postId;
        var cmtBox = $(this).parent().parent().parent().find("div[class='post-box-comment']").find("div[class='cmt-box']");
     
        $.get(path, function(data, status) {
            data.forEach(function(comment) {
                displayComment(cmtBox, comment);
            });
        });
        $(this).parent().parent().parent().find("div[class='post-box-comment']").find("div[id='hiddenCommentBox']").show();
        $(this).parent().parent().parent().find("div[class='post-box-comment']").find("div[id='hiddenCommentBox']")
            .find("form").find("input").focus();
        $(this).parent().find('input').val('false');
        return false;
    });



    $('.comment-form').submit(function() {
      
        var comment =  $(this).children().val();
        var postId = $(this).parent().parent().parent().find("div[id='contain-id']").find('input').val();

        var time = new Date().toISOString();

        
        var cmtDiv = document.createElement('div');
        cmtDiv.setAttribute('class', 'row comment-box');
        
        var nameTag = document.createElement('a');
        nameTag.setAttribute('class', 'cmt');
        nameTag.setAttribute('href', '/profile' + Cookies.get('userId'));
        nameTag.innerHTML = Cookies.get('fullname');

        var contentP = document.createElement('p');
        contentP.innerHTML = comment;

        cmtDiv.append(nameTag);
        cmtDiv.append(contentP);
       
        var timeP = document.createElement('P');
        timeP.innerHTML = time;

        var cmtBox = $(this).parent().parent().parent().find("div[class='post-box-comment']").find("div[class='cmt-box']");

        cmtBox.append(cmtDiv);
        cmtBox.append(timeP);

        var comment = {
            content: $(this).children().val(),
            time: new Date().toISOString(),
            userId: {
                _id: Cookies.get('userId'),
                fullname: Cookies.get('fullname')
            },
        };
        var cmtBox = $(this).parent().parent().parent().parent().find("div[class='post-box-comment']").find("div[class='cmt-box']");

        displayComment(cmtBox, comment);

        $(this).children().val('');

        $.post('/comment', {
            postId: $(this).parent().parent().parent().parent().find("div[id='contain-id']").find("input").val(),
            time: comment.time,
            comment: comment.content
        }, function(data, status) {
            console.log(data, status);
        });
        return false;
    });






    $('#post-form').submit(function() {
        var post = $('#post-content').val();
        $.post('/createPost', {
            time: new Date().toISOString(),
            content: post
        }, function(data, status) {
            window.alert('post saved!');
        });
        return false;
    });


















    // $.get('/getPosts', function(data) {
    //     var htm = "";
    //     data.forEach(function(post) {
    //         htm += "<div class='well post-box rounded'>";
    //         htm += "<div class='post-box-content rounded' id='contain-id'>";
    //         htm += "<input type='hidden' value='" + post._id + "'>";
    //         htm += "<div class='well row' style='flex-wrap: nowrap !important; margin: 0 !important;'>";
    //         htm += "<img src='/images/user-icon.jpg' class='rounded-circle icon-user' alt=''>";
    //         htm += "<div class='col-md-6'>";
    //         htm += "<a href='/profile/" + post.userId._id + "'>" + post.userId.fullname + "</a>";
    //         htm += "<h6>" + post.time + "</h6>";
    //         htm += "</div>"
    //         htm += "</div>";
    //         htm += "<div class='well'>";
    //         htm += post.content;
    //         htm += "</div>";
    //         htm += "<hr>";
    //         htm += "<div class='row like-cmt-share'>";
    //         htm += "<button class='col-md-4'>";
    //         htm += "<img src='/images/like-icon.png' style='width: 30px; height: 30px;' alt=''>";
    //         htm += "Like";
    //         htm += "</button>";
    //         htm += "<button class='col-md-4 get-comment-btn'>";
    //         htm += "<img src='/images/comment-icon.png' style='width: 30px; height: 30px;' alt=''>";
    //         htm += "Comment";
    //         htm += "</button>";
    //         htm += "<input type='hidden' value='true'>";
    //         htm += "<button class='col-md-4'>";
    //         htm += "<img src='/images/share-icon.png' style='width: 30px; height: 30px;' alt=''>";
    //         htm += "Share";
    //         htm += "</button>";
    //         htm += "</div>";
    //         htm += "</div>"
    //         htm += "<div class='post-box-comment'>";
    //         htm += "<div class='cmt-box'></div>";
    //         htm += "<div id='hiddenCommentBox' class='row icon-cmt-box' style='display: none;'>";
    //         htm += "<img src='http://localhost:3000/images/user-icon.jpg' class='rounded-circle icon-user-small' alt=''>";
    //         htm += "<div class='hidden-div'>";
    //         htm += "<form class='comment-form'>";
    //         htm += "<input class='input-comment' type='text' placeholder='Write comment ...'>"
    //         htm += "</form>";
    //         htm += "</div>";
    //         htm += "</div>";
    //         htm += "</div>";
    //         htm += "</div>";
    //     })
    //     console.log(htm);
    // });
});