var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var UserInfoSchema = new Schema({
    username: String,
    password: String,
    fullname: String,
    friends: [{type: Schema.ObjectId, ref: 'UserInfo'}],
    talkIds: [{type: Schema.ObjectId, ref: 'Talk'}],
    postIds: [{type: Schema.ObjectId, ref: 'Post'}],
    friendRequestIds: [{type: Schema.ObjectId, ref: 'UserInfo'}]
});

UserInfoSchema.statics.createRelationship = function(first, second) {
    var User = mongoose.model('UserInfo', UserInfoSchema);
    User.update({
        '_id': first
    }, {
        $push: {
            friends: second
        }
    }, function(err, raw) {
        if (err) return console.error(err);
    });

    User.update({
        '_id': second
    }, {
        $push: {
            friends: first
        }
    }, function(err, raw) {
        if (err) return console.error(err);
    });
    
}

UserInfoSchema.statics.saveFriendRequest = function(first, second) {
    var User = mongoose.model('UserInfo', UserInfoSchema);
    User.update({
        '_id': first
    }, {
        $push: {
            friendRequestIds: second
        }
    }, function(err, raw) {
        if (err) return console.error(err);

    });

    User.update({
        '_id': second
    }, {
        $push: {
            friendRequestIds: first
        }
    }, function(err, raw) {
        if (err) return console.error(err);
    });
    
}

UserInfoSchema.statics.removeFriendRequestId = function(userId, friendRequestId) {
    var User = mongoose.model('UserInfo', UserInfoSchema);
    User
        .update({
            '_id': userId
        }, {
            $pull: {
                friendRequestIds: friendRequestId
            }
        }, function(err, raw) {
            if (err) return console.error(err);
        });
}

UserInfoSchema.plugin(passportLocalMongoose);

UserInfo = mongoose.model('UserInfo', UserInfoSchema);

module.exports = exports = UserInfo;