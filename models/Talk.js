var mongoose = require('mongoose');
var Schema = monngoose.Schema;

var TalkSchema = new Schema({
    messages: [{
        userId: {type: Schema.ObjectId, ref: 'UserInfo', require: true},
        time: {type: Date, require: true},
        content: {type: String, require: true}
    }]
});

var Talk = mongoose.model('Talk', TalkSchema);
module.exports = Talk;