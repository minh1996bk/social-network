var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
    userId: {type: Schema.ObjectId, ref: 'UserInfo', require: true},
    time: {type: String, require: true},
    content: {type: String, require: true},
    comments: [{
        userId: {type: Schema.ObjectId, ref: 'UserInfo', require: true},
        time: {type: String, require: true},
        content: {type: String, require: true}
    }]
});

var Post = mongoose.model('Post', PostSchema);
module.exports = Post;