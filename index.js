module.exports = function(io) {
    var express = require('express');
    var bodyParser = require('body-parser');
    var cookieParser = require('cookie-parser');
    var session = require('express-session');
    var passport = require('./lib/auth');
    var morgan = require('morgan');
    var errorhandler = require('errorhandler');
    var flash = require('connect-flash');

    const PORT = process.env.PORT || 3000;
    var isProduction = process.env.NODE_ENV === 'production';
    var app = express();



    app.use(express.static('./public'));

    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
    app.use(flash());
    app.use(cookieParser());
    app.use(session({
        secret : "secret",
        saveUninitialized: true,
        resave: true
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.set('view engine', 'ejs');
    app.set('views', './views');
    app.use(express.static('./public'));

    var mongoose = require('mongoose');
    if (!isProduction) {
        app.use(errorhandler());
        console.log("app's running in dev mode");
    }
    var redis = require('redis');


    // client.set('hi', 'xin chao', (err, rep) => {
    //     if (err) return console.log(err);
    //     return console.log(rep);
    // });


    if (isProduction) {
        mongoose.connect(process.env.PROD_MONGODB);
        

    } else {
        mongoose.connect('mongodb://localhost/webdb');
        var client = redis.createClient();
        // client.on('connect', () => {
        //     console.log('connected');
        // })
        client.on('error', (err) => {
            if (err) {
                console.error(err);
                return process.exit(0);
            }
            return console.log('connected!');
        });
    }



    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'Mongodb connection error!'));




    var router = require('./routes/router')(io);
    app.use('/', router);



    /// catch 404 and forward to error handler
    // app.use(function(req, res, next) {
    //     var err = new Error('Not Found');
    //     err.status = 404;
    //     next(err);
    //   });
    
    //   /// error handlers
    
    //   // development error handler
    //   // will print stacktrace
    //   if (!isProduction) {
    //     app.use(function(err, req, res, next) {
    //       console.log(err.stack);
    
    //       res.status(err.status || 500);
    
    //       res.json({'errors': {
    //         message: err.message,
    //         error: err
    //       }});
    //     });
    //   }
    
    //   // production error handler
    //   // no stacktraces leaked to user
    //   app.use(function(err, req, res, next) {
    //     res.status(err.status || 500);
    //     res.json({'errors': {
    //       message: err.message,
    //       error: {}
    //     }});
    //   });

    return app;
}



