
module.exports = function(io, server) {
    var redis = require('redis');
    io = require('socket.io')(server);
    var client = redis.createClient();

    io.on('connection', function(socket) {
        console.log('user connected!');


        socket.on('register', (data) => {
            client.set(data.id, socket.id, (err, rep) => {
                if (err) return console.error(err);
                console.log(rep + ' socketid and userid respectively saved!');
            });
        });

        socket.on('getId', (data) => {
            client.get(data.id, (err, rep) => {
                if (err) return console.error(err);
                io.emit('returnId', {id: rep});
            })
        });

        socket.on('chat', (data) => {
            client.get(data.friendId, (err, rep) => {
                if (err) return console.error(err);
                io.to(rep).emit('chat', data.fullname + ' say ' + data.msg);
            })
        })
    });
}