var passport = require('passport');
var Strategy = require('passport-local').Strategy;  
var User = require('../models/UserInfo');

passport.use(new Strategy(
    function(username, password, done) {
      User.findOne({'username': username}, function(err, user) {
       
        if (err) { 
            return done(err); 
        }
        if (!user) { 
            return done(null, false); 
        }
        if (password != user.password) { 
            return done(null, false); 
        }
       
        return done(null, user);
      });
    })
);

passport.serializeUser(function(user, done) {
    done(null, user.id);
});
  
passport.deserializeUser(function(id, done) {
    User.findById({'_id': id}, function (err, user) {
        if (err) {
            return done(err); 
        }
        done(null, user);
    });
});

module.exports = passport;