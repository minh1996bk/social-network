var mongoose = require("mongoose");
var passport = require('../lib/auth');
var User = require('../models/UserInfo');
var Post = require('../models/Post');


var userController = {};

userController.getPosts = function(req, res) {
  Post.find({})
    .populate('userId', {'_id': 1, 'fullname': 1})
    .exec(function(err, data) {
      if (err) return handleError(err);
      res.json(data);
    });
}

userController.viewProfile = function(req, res) {
  var id = req.params.id;
  User
    .findById({'_id': id})
    .populate('postIds')
    .populate('friends')
    .exec(function(err, data) {
      if (err) return console.error(err);
      res.render('profile', {user: data});
    });
}

userController.getComments = function(req, res) {

  var postId = req.params.id;


  Post
    .findById({'_id': postId}, {'comments': 1, '_id': 0})
    .populate('comments.userId', {'_id': 1, 'fullname': 1})
    .exec(function(err, data) {
      if (err) return console.error(err);
      
      res.json(data.comments);
    });  
  
}





userController.getFriendTips = function(req, res) {

  if (!req.session || !req.session.passport || !req.session.passport.user) {
    res.sendStatus(500);
  }

  var userId = req.session.passport.user;




function getListFriends(id) {
    User.findOne({'_id': id}, 'friends friendRequestIds', function(err, data) {
      if (err) return handleError(err);
 
      data.friends.push(id);
      var friendIds = data.friends.concat(data.friendRequestIds);
   
      User
        .find({'_id': {$nin: friendIds}}, {'_id': 1, 'fullname': 1})
        .exec(function(err, tips) {
          if (err) return handleError(err);
          return res.json(tips);
        });
      
    });
  }

  getListFriends(userId);
}

userController.getFriendRequest = function(req, res) {
  var userId = req.session.passport.user;
  User
    .findById({'_id': userId}, {'friendRequestIds': 1})
    .populate('friendRequestIds', {'_id': 1, 'fullname': 1})
    .exec(function(err, data) {
      if (err) return handleError(err);
      
      res.json(data.friendRequestIds);
    });
}

module.exports = userController;