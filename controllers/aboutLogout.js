var mongoose = require("mongoose");
var passport = require('../lib/auth');
var User = require('../models/UserInfo');

var aboutLogout = {};
// Go to registration page
aboutLogout.logout = function(req, res) {
    req.logout();
    res.redirect('/login');
};

module.exports = aboutLogout;