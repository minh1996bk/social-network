var mongoose = require("mongoose");
var passport = require('../lib/auth');
var User = require('../models/UserInfo');


var aboutRegister = {};

// Post registration
aboutRegister.doRegister = function(req, res) {
    var registerUser = new User({
      username: req.body.username,
      password: req.body.password,
      fullname: req.body.fullname
    });
    registerUser.save(function(err) {
      if (err) console.error(err);
      else {
        console.log("new user's registered");
        req.login(registerUser, function(err) {
          if (err) console.error(err);
          else {
            return res.redirect('/');
          }
        });
      }
    });
};

aboutRegister.register = function(req, res) {
    res.render('register');
}

module.exports = aboutRegister;