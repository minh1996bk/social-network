module.exports = function(io) {
    var sr = {};
    var User = require('../models/UserInfo');
    var Post = require('../models/Post');


    function handleError(err) {
        return res.render('error', {error: err})
    }

    sr.createPost = function(req, res) {
        if (!req.session) {
            return res.redirect('/');
        }

        if (!req.session.passport) {
            return res.redirect('/');
        }

        if (!req.session.passport.user) {
            return res.redirect('/');
        }

        var time = req.body.time;
        var content = req.body.content;
        var userId = req.session.passport.user;

        var post = new Post({
            userId: req.session.passport.user,
            time: time,
            content: content
        });
        post.save(function(err) {
            if (err) return console.error(err);
            User.update({
            '_id': userId
            }, {
            $push: {
                postIds: post.id
            }
            }, function(err, raw) {
            if (err) return console.error(err);
            res.sendStatus(200);
            });
        });
    }

    sr.comment = function(req, res) {
        var newComment = {
          userId: req.session.passport.user,
          time: req.body.time,
          content: req.body.comment
        }
        Post.update({
          '_id': req.body.postId
        }, {
          $push: {
            comments: newComment
          }
        }, function(err, raw) {
          if (err) {
            console.error(err)
            return res.sendStatus(500);
          } else return res.sendStatus(200);
        });
    }
    
    sr.sendFriendRequest = function(req, res) {
        var first = req.body.friendId; 
        var second = req.session.passport.user;
        User.saveFriendRequest(first, second);
        res.sendStatus(200);
    }

    sr.acceptFriendRequest = function(req, res) {
        var first = req.body.friendId; 
        var second = req.session.passport.user;
        User.createRelationship(first, second);
        User.removeFriendRequestId(first, second);
        User.removeFriendRequestId(second, first);
        res.sendStatus(200);
    }


    return sr;
}