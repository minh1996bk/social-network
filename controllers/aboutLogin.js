var mongoose = require("mongoose");
var passport = require('../lib/auth');
var User = require('../models/UserInfo');
var Post = require('../models/Post');

var aboutLogin = {};
aboutLogin.home = function(req, res) {
    if (!req.session) return res.sendStatus(500);
    
    if (!req.session.passport) return res.render('login');
  
    if (!req.session.passport.user) return res.sendStatus(500);
   
    var userId = req.session.passport.user;
  
    // User
    //   .findById({'_id': userId})
    //   .populate('postIds', {'comments': 0})
    //   .populate('friends', {'fullname': 1, '_id': 1})
    //   .exec(function(err, data) {
    //     if (err) return console.error(err);
    //     return res.render('home', {user: data});
    //   });
    User
    .findById({'_id': userId}, {'postIds': 0})
    .populate('friends', {'fullname': 1, '_id': 1})
    .exec(function(err, data) {
        if (err) return console.error(err);
        Post.find({})
            .populate('userId', {'_id': 1, 'fullname': 1})
            .exec(function(err, posts) {
            if (err) return handleError(err);
                data.postIds = posts;
                return res.render('home', {user: data});
                
            });

      
    });
};

aboutLogin.doLogin = function(req, res) {
    passport.authenticate('local')(req, res, function() {
        res.redirect('/');
    });
};

aboutLogin.login = function(req, res) {
    res.render('login');
};
  
module.exports = aboutLogin;